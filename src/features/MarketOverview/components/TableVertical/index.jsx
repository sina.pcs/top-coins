/**
 * vertical table for showing data in mobile
 */
// @flow
import React from 'react';
import { coinsLabelFormatter } from 'store/InitialState/coins';
import './styles.scss'
import type { CoinRecord } from 'store/InitialState/coins';

type Props = {
  data: Array<CoinRecord>
};

const TableVertical = ({data}: Props) => <div className={'tableVertical'}>
  {data.map((crypto => <div key={crypto.id}>
    {Object.keys(coinsLabelFormatter)
      .map(column => <div className={'row'} key={crypto.id + column}>
          <div>
            {coinsLabelFormatter[column].label}
          </div>
          <div>
            {coinsLabelFormatter[ column ].formatter(crypto[ column ])}
          </div>
        </div>
      )}
  </div>))}
</div>

export default TableVertical;
