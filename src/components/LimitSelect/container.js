// @flow
import {connect} from 'react-redux';
import LimitSelect from './component';
import type { Store } from 'store/InitialState';
import Selectors from 'store/selectors';
import { bindActionCreators } from 'redux';
import Actions from 'store/Action';

const mapStateToProps = (state: Store) => ({
  limit: Selectors.coinsLimit(state)
})

const mapDispatchToProps = dispatch => bindActionCreators({fetchCoinsData: Actions.coinActions.fetchData}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(LimitSelect);
