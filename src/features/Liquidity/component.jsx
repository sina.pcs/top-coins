// @flow
import React from 'react';
import {
  ResponsiveContainer, ScatterChart, Scatter, XAxis, YAxis, ZAxis, CartesianGrid, Tooltip, Label,
} from 'recharts';
import type { CoinRecord } from 'store/InitialState/coins';
import CustomTooltip from './CustomTooltip'
import {coinsLabelFormatter} from 'store/InitialState/coins';
import CustomAxisTick from './CustomAxisTick';

type Props = {
  data: Array<CoinRecord>,
}

export default class Liquidity extends React.Component<Props> {
  render() {
    const {data} = this.props;

    // empty data
    if (!data.length) {
      return null;
    }

    return (
      <ResponsiveContainer width={'100%'}
                           height={'100%'}>
        <ScatterChart
          margin={{
            top: 20, right: 0, bottom: 20, left: 0,
          }}>
          <CartesianGrid />

          <XAxis
            type="number"
            dataKey="marketCap"
            name={coinsLabelFormatter.marketCap.label}
            tickFormatter={coinsLabelFormatter.marketCap.formatter}
            tick={<CustomAxisTick axis={'x'} formatter={coinsLabelFormatter.volume24h.formatter} />}
          >

            <Label value={coinsLabelFormatter.marketCap.label} fontSize={12} offset={0} position="bottom" />
          </XAxis>

          <YAxis
            type="number"
            dataKey="volume24h"
            name={coinsLabelFormatter.volume24h.label}
            tick={<CustomAxisTick axis={'y'} formatter={coinsLabelFormatter.volume24h.formatter} />}
          >
            <Label
              value="Volume"
              position="insideLeft"
              fontSize={12}
            />
          </YAxis>

          <ZAxis
            type="number"
            dataKey="priceChange24h"
            name={coinsLabelFormatter.priceChange24h.label}
            range={[100, 500]}
            scale={'linear'} />

          <Tooltip content={<CustomTooltip/>}/>
          <Scatter name="Liquidity" data={data} fill="rgba(0,0,0,0.3)" shape="circle" />
        </ScatterChart>
      </ResponsiveContainer>
    );
  }
}
