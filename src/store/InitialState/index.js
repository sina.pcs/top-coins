// @flow

import coins from './coins';
import type { CoinsStore } from './coins';

// Store type
export type Store = {
  coins: CoinsStore
}

// initialState which integrates all initial states
const initialState: Store = {
  coins,
}

export default initialState;
