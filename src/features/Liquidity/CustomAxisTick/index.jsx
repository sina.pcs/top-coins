import React from 'react';
import MediaQuery from 'components/MediaQuery';

// custom Tick for recharts which will be rendered only on Desktop
const CustomAxisTick = ({axis, formatter, x, y, payload}) => {
  const isXAxis = axis === 'x';
  return (<MediaQuery.Desktop>
      <g transform={`translate(${x},${y})`}>
        <text x={0} y={0} dx={isXAxis ? 60 : -10} dy={isXAxis ? 10 : 0} textAnchor="end" fill="#666" fontSize={12}
              transform={`rotate(${isXAxis ? 0 : -80})`}>{formatter(payload.value)}</text>
      </g>
    </MediaQuery.Desktop>
  );
};

export default CustomAxisTick;
