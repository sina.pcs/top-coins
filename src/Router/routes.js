/**
 * the project routes which is using in SwitchRoutes component
 */

import MarketOverview from 'features/MarketOverview'
import Liquidity from 'features/Liquidity';

export default [
  {
    path: '/liquidity',
    component: Liquidity
  },
  {
    path: '/',
    component: MarketOverview
  }
]
