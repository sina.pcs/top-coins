// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')

beforeEach(() => {
  cy.server();
  cy.route(
    "GET",
    `${Cypress.config("apiBaseUrl")}/cryptocurrency/listings/latest`,
    "fx:coins-unlimited",
  );
  cy.route(
    "GET",
    `${Cypress.config("apiBaseUrl")}/cryptocurrency/listings/latest?limit=10`,
    "fx:coins-10",
  );
  cy.route(
    "GET",
    `${Cypress.config("apiBaseUrl")}/cryptocurrency/listings/latest?limit=50`,
    "fx:coins-50",
  );
})
