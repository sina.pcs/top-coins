/*
  create different components for rendering children in specific breakpoints
 */

import { useMediaQuery } from 'react-responsive'

export const breakpoints = {
  desktop: {minWidth: 992},
  tablet: {minWidth: 768, maxWidth: 991},
  mobile: {maxWidth: 767},
  notMobile: {minWidth: 768},
}

const Desktop = ({ children }) => {
  const isDesktop = useMediaQuery(breakpoints.desktop)
  return isDesktop ? children : null
}
const Tablet = ({ children }) => {
  const isTablet = useMediaQuery(breakpoints.tablet)
  return isTablet ? children : null
}
const Mobile = ({ children }) => {
  const isMobile = useMediaQuery(breakpoints.mobile)
  return isMobile ? children : null
}
const NotMobile = ({ children }) => {
  const isNotMobile = useMediaQuery(breakpoints.notMobile)
  return isNotMobile ? children : null
}

export default {
  Desktop,
  Tablet,
  Mobile,
  NotMobile,
}
