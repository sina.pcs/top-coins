describe("MarketOverview", () => {
  it('should render all rows in table', function () {
    cy.visit('/');
    cy.get('table > tbody > tr').should('have.length', 3);
    cy.get('#limitSelect').select('50');
    cy.get('table > tbody > tr').should('have.length', 2);
    cy.get('#limitSelect').select('10');
    cy.get('table > tbody > tr').should('have.length', 1);
  });
  it('should render columns in table correctly', function () {
    cy.visit('/');
    cy.get('table > thead > tr > th').should('have.length', 6);
    cy.get('table > thead > tr > th:nth-child(1)').should('contain', 'Rank');
    cy.get('table > thead > tr > th:nth-child(2)').should('contain', 'Name');
    cy.get('table > thead > tr > th:nth-child(3)').should('contain', 'Price');
    cy.get('table > thead > tr > th:nth-child(4)').should('contain', 'Price Change (24h)');
    cy.get('table > thead > tr > th:nth-child(5)').should('contain', 'Market Cap');
    cy.get('table > thead > tr > th:nth-child(6)').should('contain', 'Volume (24h)');

    cy.get('table > tbody > tr:first-child > td').should('have.length', 6);
    cy.get('table > tbody > tr:first-child > td:nth-child(1)').should('contain', '1');
    cy.get('table > tbody > tr:first-child > td:nth-child(2)').should('contain', 'Bitcoin');
    cy.get('table > tbody > tr:first-child > td:nth-child(3)').should('contain', '$10,000.50');
    cy.get('table > tbody > tr:first-child > td:nth-child(4)').should('contain', '5%');
    cy.get('table > tbody > tr:first-child > td:nth-child(5)').should('contain', '$200,000.00');
    cy.get('table > tbody > tr:first-child > td:nth-child(6)').should('contain', '$100,000.00');
  });
  it('should render vertical mode switch which can change the table mode to vertical on mobile', function () {
    cy.viewport(767, 1000); // 767px is our max-width breakpoint for mobiles
    cy.visit('/');
    cy.get('#checkbox').should('exist');
    cy.get('#checkbox').click().get('.tableVertical').should('exist');
    cy.get('.tableVertical > div').should('have.length', 3);
    cy.get('table').should('not.exist');
  })
});
