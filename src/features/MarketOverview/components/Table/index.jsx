/**
 * regular table for showing data
 */
// @flow
import React from 'react';
import { coinsLabelFormatter } from 'store/InitialState/coins';
import './styles.scss'
import type { CoinRecord } from 'store/InitialState/coins';

type Props = {
  data: Array<CoinRecord>
};

const Table = ({data}: Props) => <div className={'tableContainer'}>
  <table className={'table'} cellSpacing={0}>
    <thead>
    <tr>
      {Object.values(coinsLabelFormatter)
        .map(column => <th key={column.label}>{column.label}</th>)}
    </tr>
    </thead>
    <tbody>
    {data.map((crypto => <tr key={crypto.id}>
      {Object.keys(coinsLabelFormatter)
        .map(column => <td key={crypto.id + column}>{coinsLabelFormatter[ column ].formatter(crypto[ column ])}</td>)}
    </tr>))}
    </tbody>
  </table>
</div>

export default Table;
