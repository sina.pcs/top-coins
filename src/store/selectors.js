// @flow
import type { Store } from './InitialState';

// selectors for easy select data from store
export default {
  coinsLimit: (state: Store) => state.coins.limit,
  coinsData: (state: Store) => state.coins.data,
  coinsIsFetching: (state: Store) => state.coins.isFetching,
  coinsError: (state: Store) => state.coins.error,
}
