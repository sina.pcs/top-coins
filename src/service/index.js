/**
 * We create an instance of our services and export them here
 */

import CryptocurrencyServic from './Cryptocurrency';

export default {
  cryptocurrency: new CryptocurrencyServic(),
}
