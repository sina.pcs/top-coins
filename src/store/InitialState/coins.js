// @flow

// CoinRecord type
import toCurrency from 'helpers/toCurrency';
import toPercent from 'helpers/toPercent';

export type CoinRecord = {
  id: string,
  rank: string,
  name: string,
  price: number,
  priceChange24h: number,
  marketCap: number,
  volume24h: number,
}

/* object by which we can define the columns we need to show in table
   key: CoinRecord attributes
   {label: title of columns, formatter: formatter function}
*/
export const coinsLabelFormatter = Object.freeze({
  rank: {
    label: 'Rank',
    formatter: value => value,
  },
  name: {
    label: 'Name',
    formatter: value => value,
  },
  price: {
    label: 'Price',
    formatter: toCurrency
  },
  priceChange24h: {
    label: 'Price Change (24h)',
    formatter: toPercent
  },
  marketCap: {
    label: 'Market Cap',
    formatter: toCurrency
  },
  volume24h: {
    label: 'Volume (24h)',
    formatter: toCurrency,
  },
});

// Coins initial state type
export type CoinsStore = {
  limit: string,
  data: Array<CoinRecord>,
  isFetching: boolean,
  error: any,
}

// Coins initial state
const initialState: CoinsStore = {
  limit: '',
  data: [],
  isFetching: false,
  error: null,
}

export default initialState;
