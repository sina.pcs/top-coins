describe("Liquidity", () => {
  it('should render data in scatter plot ', function () {
    cy.visit('/liquidity');
    cy.get('path[name=Ethereum]').should('exist');
  });
  it('should render tooltip on hover elements in plot and show currency details ', function () {
    cy.visit('/liquidity');
    cy.get('path[name=Ethereum]').trigger('mouseover');
    cy.get('.recharts-tooltip-wrapper').should('exist')
      .should('contain', 'Name: Ethereum')
      .should('contain', 'Market Cap: $100,000.00')
      .should('contain', 'Volume (24h): $50,000.00')
      .should('contain', 'Price Change (24h): 1%');
  });
  it('should render elements in plot in different sizes based on their price change value in 24h', function () {
    cy.visit('/liquidity');
    cy.get('path[name=Bitcoin]').then(bitcoin => {
      cy.get('path[name=Ethereum]').then(ether => {
        expect(bitcoin.attr('width')).is.greaterThan(ether.attr('width'))
        cy.get('path[name=Litecoin]').then(litecoin => {
          expect(ether.attr('width')).is.greaterThan(litecoin.attr('width'))
        });
      })
    });
  });
  it('should re-render plot on limit change', function () {
    cy.visit('/liquidity');
    cy.get('.recharts-scatter path').should('have.length', 3);
    cy.get('#limitSelect').select('50');
    cy.get('.recharts-scatter path').should('have.length', 2);
    cy.get('#limitSelect').select('10');
    cy.get('.recharts-scatter path').should('have.length', 1);
  });
});
