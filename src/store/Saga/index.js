import {all} from 'redux-saga/effects'
import coinsSaga from "./coins";

// integrate all sagas
function* mySaga() {
  yield all([
    coinsSaga()
  ])
}

export default mySaga;
