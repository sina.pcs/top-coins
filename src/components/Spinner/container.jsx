// @flow
import Spinner from "./component";
import {connect} from 'react-redux';
import type { Store } from 'store/InitialState';
import Selectors from 'store/selectors'

const mapStateToProps = (state: Store) => ({
  isFetching: Selectors.coinsIsFetching(state)
})

export default connect(mapStateToProps)(Spinner);
