import initialState from "store/InitialState";
import { actionTypes } from "store/Action";

const coinsReducer = (state = initialState.coins, action) => {
  switch (action.type) {
    case actionTypes.COINS_FETCH_DATA_REQUEST:
      return {
        ...state,
        error: null,
        limit: action.limit,
        isFetching: true,
      }
    case actionTypes.COINS_FETCH_DATA_SUCCESS:
      return {
        ...state,
        data: action.data,
        isFetching: false,
      }
    case actionTypes.COINS_FETCH_DATA_FAIL:
      return {
        ...state,
        error: action.error,
        limit: action.previousLimit,
        isFetching: false,
      }
    default:
      return state;
  }
}

export default coinsReducer;
