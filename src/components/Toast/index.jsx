/**
 * Toast component for showing some messages as toast
 */
import React from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const Toast = () => <ToastContainer/>;

export default Toast;
