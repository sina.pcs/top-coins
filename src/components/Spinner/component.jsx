import React from 'react';
import './styles.scss';

const Spinner = ({isFetching}) => isFetching ? <div className={'spinner'}><div className={'loader'}/></div> : null;

export default Spinner;
