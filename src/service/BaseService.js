// @flow

/**
 * BaseService for creating an instance of axios with some default config
 */

import axios from 'axios';
import {toast} from 'react-toastify';

// simple error handler for showing toast on errors
// we can have different reactions on different error statuses here
const errorHandler = (e) => {
  if(e.message) {
    toast.error(e.message, {
      position: toast.POSITION.BOTTOM_RIGHT
    });
    throw e;
  }
  throw e;
}

export default class BaseService {
  /* the resource path in request url e.g. '/cryptocurrency' or '/exchange'
     we have to set it in the Service classes which are extending this Class
  */
  resourcePath: string = '';
  client: axios.Axios;
  constructor() {
    // We are using REACT_APP_COINS_API_URL as our baseUrl
    if (!process.env.REACT_APP_COINS_API_URL) {
      throw new Error('Please set REACT_APP_COINS_API_URL ENV as CoinMarketCap base url');
    }
    // We are using REACT_APP_COINS_API_TOKEN as our token of requests
    // It was more secure to use token in our server but because we are just doing some frontend tests I used herokuapp to enable cross-origin request
    if (!process.env.REACT_APP_COINS_API_TOKEN) {
      throw new Error('Please set REACT_APP_COINS_API_TOKEN ENV as CoinMarketCap token');
    }

    // Setting baseURL and headers
    this.client = axios.create({
      baseURL: process.env.REACT_APP_COINS_API_URL,
      headers: {
        'X-CMC_PRO_API_KEY': process.env.REACT_APP_COINS_API_TOKEN,
      }
    });
  }

  /**
   * Get method of our client
   * @param {string} path - The path after baseURL and resourcePath. e.g. '/listing/latest'
   */
  sendGet = async (path: string): Promise<any> => {
    const finalPath = this.resourcePath + path;
    return this.client.get(finalPath).then(res => res.data).catch(errorHandler);
  }
}
