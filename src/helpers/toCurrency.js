// @flow
/**
 * function for changing the format of number to currency
 * @param {number} price
 * @param {string} [currency=USD]
 */
export default function (price: number, currency: string = 'USD') {
  return new Intl.NumberFormat('en-US', { style: 'currency', currency }).format(price);
}
