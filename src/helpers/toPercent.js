// @flow
/**
 * Simple function for adding a % sign after number
 * @param {number} change
 */
export default function (change: number) {
  return `${change}%`;
}
