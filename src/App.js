import React from 'react';
import { Provider } from 'react-redux';
import Toast from 'components/Toast'
import Router from './Router';
import {store} from './store';

function App() {
  return (
    <Provider store={store}>
        <Router/>
        <Toast/>
    </Provider>
  );
}

export default App;
