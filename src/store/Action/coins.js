// @flow

/**
 * coins action types and action creators
 */
import type { CoinRecord } from 'store/InitialState/coins';

export const coinsActionsTypes = {
  COINS_FETCH_DATA_REQUEST: 'COINS_FETCH_DATA_REQUEST',
  COINS_FETCH_DATA_SUCCESS: 'COINS_FETCH_DATA_SUCCESS',
  COINS_FETCH_DATA_FAIL: 'COINS_FETCH_DATA_FAIL',
};

export const coinActions = {
  /**
   * fetch coins list action creator
   * @param {string} [limit = ''] - number of elements to get
   */
  fetchData: (limit = '') => ({
    type: coinsActionsTypes.COINS_FETCH_DATA_REQUEST,
    limit,
  }),
  /**
   * fetch coins list success action creator
   * set the received data
   * @param {string} data
   */
  fetchDataSuccess: (data: Array<CoinRecord>) => ({
    type: coinsActionsTypes.COINS_FETCH_DATA_SUCCESS,
    data,
  }),
  /**
   * fetch coins list fail action creator
   * revert the limit back to previous one
   * set the received error
   * @param {string} error
   * @param {string} previousLimit
   */
  fetchDataFail: (error: string, previousLimit: string) => ({
    type: coinsActionsTypes.COINS_FETCH_DATA_FAIL,
    error,
    previousLimit,
  })
};
