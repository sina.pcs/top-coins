import React from 'react';
import DefaultTooltipContent from 'recharts/lib/component/DefaultTooltipContent';
import {coinsLabelFormatter} from 'store/InitialState/coins';

// Tooltip with customized payload
const CustomTooltip = props => {
  if (!props.active) {
    return null
  }
  const {name, marketCap, volume24h, priceChange24h} = props.payload[0].payload;
  const newPayload = [
    {
      name: coinsLabelFormatter.name.label,
      value: name,
    },
    {
      name: coinsLabelFormatter.marketCap.label,
      value: coinsLabelFormatter.marketCap.formatter(marketCap),
    },
    {
      name: coinsLabelFormatter.volume24h.label,
      value: coinsLabelFormatter.volume24h.formatter(volume24h),
    },
    {
      name: coinsLabelFormatter.priceChange24h.label,
      value: coinsLabelFormatter.priceChange24h.formatter(priceChange24h),
    },
  ];

  // we render the default, but with our overridden payload
  return <DefaultTooltipContent {...props} separator={': '} payload={newPayload} />;
};

export default CustomTooltip;
