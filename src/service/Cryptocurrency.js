// @flow

/**
 * CryptocurrencyService which extends BaseService
 */

import BaseService from './BaseService';
import type { CoinRecord } from 'store/InitialState/coins';

type ListingResponse = {
  status: Object,
  data: Array<CoinRecord>
}

export default class CryptocurrencyService extends BaseService{
  // set resourcePath to '/cryptocurrency'
  resourcePath = '/cryptocurrency';

  /**
   * sending get requests to '/listings/latest' path which can have some limits too by using query string
   * normalizing the received data and return it
   * @param {string} limit - number of elements to get
  */
  getList = async (limit: string): Promise<Array<CoinRecord>> => {
    return this.sendGet(`/listings/latest${limit && '?limit=' + limit}`).then((res: ListingResponse) => {
      return res.data.map(this.normalizeRecord);
    })
  }

  /**
   * normalizing the received object and return it
   * @param {object} currency - the currency received from api
  */
  normalizeRecord = (currency: Object): CoinRecord => {
    let record = {
      id: currency.id,
      rank: currency.cmc_rank,
      name: currency.name,
    };
    if(currency.quote){
      record = {
        ...record,
        price: currency.quote.USD.price,
        priceChange24h: currency.quote.USD.percent_change_24h,
        marketCap: currency.quote.USD.market_cap,
        volume24h: currency.quote.USD.volume_24h,
      }
    }
    return record;
  }
}
