/**
 Spinner component to show loader in many situations
 for now it's just rendering based on isFetching value of coins store
 */

export {default} from './container';
