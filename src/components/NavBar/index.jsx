/**
 * NavBar is rendering the main navigation and LimitSelect component
 */

import React from 'react';
import { NavLink } from 'react-router-dom';
import './styles.scss';
import LimitSelect from 'components/LimitSelect'

const NavBar = () => (
  <nav>
    <ul>
      <li>
        <NavLink exact activeClassName={'active'} to="/">Market Overview</NavLink>
      </li>
      <li>
        <NavLink activeClassName={'active'} to="/liquidity">Liquidity</NavLink>
      </li>
    </ul>
    <LimitSelect />
  </nav>
);

export default NavBar;
