This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Steps to run and test

In the project directory:

### `install project dependencies`
    yarn


### `Set Environment Variables`

create a .env file in root of project which has these variables:
`REACT_APP_COINS_API_URL` and
`REACT_APP_COINS_API_TOKEN`
<br />
<br />
for `REACT_APP_COINS_API_URL` you can use this address: `https://cors-anywhere.herokuapp.com/https://sandbox-api.coinmarketcap.com/v1`
<br />
for `REACT_APP_COINS_API_TOKEN` you should sign up and get your token in [sandbox.coinmarketcap.com](sandbox.coinmarketcap.com)

### `Start the app in the development mode`
    yarn start

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `Test the app using Cypress`

    yarn start:test
env variables are defined for cypress tests<br />
It will be serve on [http://localhost:3001](http://localhost:3001)
You can run `yarn test:e2e` after successfully starting of app in test mode
    
    yarn test:e2e

## Description of the problem and solution.

### Problem:
[Front end engineer code challenge](https://github.com/WATTx/code-challenges/blob/master/frontend-engineer-challenge-top-coins.md)

### Solution:
Creating an app using React which can fetch data from [sandbox.coinmarketcap.com/cryptocurrency/listings/latest](sandbox.coinmarketcap.com/cryptocurrency/listings/latest)
and show the result in table or scatter plot.
It can limit data by changing the limit dropdown value

## Libraries I used
### redux / react-redux / redux-saga
for storing fetched data and have access to it anywhere
### react-router-dom
for better navigation handling
### axios
HTTP client for handling network requests
### flow
simple type checking <br />
we could have `typescript` instead of `flow` in the project
the main reason I've chosen `flow` is it's simplicity and ease of use for small projects
### SASS
CSS preprocessor <br />
I've chosen `SASS` for this project because it's already supported in `react-create-app` without any configuration
### react-responsive
it can allow us to make media-query components in React
### recharts
library for creating charts and graphs <br />
The reason I've chosen this library among a large set of graph drawing libraries: <br />
1) It's open source
2) It had three dimension scatter plot feature
3) Easy to use
### react-toastify
showing toast in react <br />
I've used it for showing network errors as toasts
### cypress
a great library for end to end testing in javascript

## Could Be Better
I can admit the UI could be better in both table and chart
1) we could work more on `table` or use a library for creating `table` to have sort based on other columns or make the table header sticky on scroll
2) we could have better UI in Navigation like using better buttons or use a custom `Select` component which can have same UI in different browsers
3) we could work more on chart UI
