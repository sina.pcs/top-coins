// @flow
import Liquidity from "./component";
import {connect} from 'react-redux';
import type { Store } from 'store/InitialState';
import Selectors from 'store/selectors'

const mapStateToProps = (state: Store) => ({
  data: Selectors.coinsData(state),
})

export default connect(mapStateToProps)(Liquidity);
