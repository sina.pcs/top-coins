/**
 * Router container which is rendering NavBar and SwitchRoutes components
 * CSS: it's responsible for keeping the <header> on top even if we have overflow in <main>
 */

import React from 'react';
import {
  BrowserRouter,
} from 'react-router-dom';
import NavBar from 'components/NavBar';
import SwitchRoutes from 'components/SwitchRoutes';
import './styles.scss';
import Spinner from 'components/Spinner';

const Router = () => (
  <BrowserRouter>
    <div className={'root'}>
      <header>
        <NavBar />
      </header>
      <main>
        <Spinner/>
        <SwitchRoutes />
      </main>
    </div>
  </BrowserRouter>
);

export default Router;
