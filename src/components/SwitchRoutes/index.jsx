/**
 * SwitchRoutes is using react-router-dom Switch/Route to handle routing
 */

import React from 'react';
import { Route, Switch } from 'react-router-dom';
import routes from 'Router/routes';

const SwitchRoutes = () => (
  <Switch>
    {routes.map(route => {
      const {component: Component, ...rest} = route;
      return <Route key={route.path} {...rest}>
        <Component />
      </Route>
    })}
  </Switch>
);

export default SwitchRoutes;
