// @flow

import React, {useState} from 'react';
import type { CoinRecord } from 'store/InitialState/coins';
import './styles.scss';
import MediaQuery from 'components/MediaQuery';
import Table from './components/Table';
import TableVertical from './components/TableVertical';

type Props = {
  data: Array<CoinRecord>,
};

const MarketOverview = ({data}: Props) => {

  // isVertical is used to show a vertical table in mobile
  const [isVertical, setIsVertical] = useState(false);

  // empty data
  if (!data.length) {
    return null;
  }

  const handleCheckboxChange = (e) => {
    setIsVertical(e.target.checked);
  }

  return (
    <>
      <MediaQuery.Mobile>
        <div className={'switchVerticalCheckbox'}>
          <input type={'checkbox'} id={'checkbox'} checked={isVertical} onChange={handleCheckboxChange} />
          <label htmlFor={'checkbox'}>Vertical mode</label>
        </div>
      </MediaQuery.Mobile>

      {!isVertical && <Table data={data} />}
      {isVertical && <TableVertical data={data} /> }
    </>
  );
}


export default MarketOverview;
