import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware, compose } from 'redux';
import reducer from './Reducer';
import mySaga from './Saga';

// redux dev tool configuration
const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

// create saga middleware
const sagaMiddleWare = createSagaMiddleware();

// array of middleware
const middleware = [
  sagaMiddleWare,
];

// enhancer to use in createStore function
const enhancer = composeEnhancers(
  applyMiddleware(...middleware),
);

// create store
export const store = createStore(reducer, enhancer);

// run sagas
sagaMiddleWare.run(mySaga);
