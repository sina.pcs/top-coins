/**
 * integrated app actions and action types
 */

import {coinsActionsTypes, coinActions} from './coins';

export const actionTypes = {
  ...coinsActionsTypes,
}

export default {
  coinActions,
}
