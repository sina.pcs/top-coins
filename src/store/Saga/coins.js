import { all, takeLatest, call, put, select } from 'redux-saga/effects';
import Actions, {actionTypes} from 'store/Action';
import Services from 'service';
import Selectors from 'store/selectors';

function* handleFetchData({limit}){
  // get previous limit from store
  const previousLimit = yield select(Selectors.coinsLimit);
  try {
    // fetch data from service
    const data = yield call(Services.cryptocurrency.getList, limit);
    // dispatch fetch success action
    yield put(Actions.coinActions.fetchDataSuccess(data));
  }
  catch (e) {
    // dispatch fetch fail action
    yield put(Actions.coinActions.fetchDataFail(e, previousLimit))
  }
}

function* fetchCoinsDataSaga() {
  // take latest dispatched coins fetch action and handle it
  yield takeLatest(actionTypes.COINS_FETCH_DATA_REQUEST, handleFetchData)
}

function* coinsSaga() {
  yield all([
    fetchCoinsDataSaga()
  ])
}

export default coinsSaga;
