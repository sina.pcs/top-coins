// @flow

import {combineReducers} from "redux";
import coins from "./coins";
import type { Store } from '../InitialState';

// combine all reducers
const reducer: Store = combineReducers({coins});

export default reducer;
