// @flow

import React, {useEffect} from 'react';
import './styles.scss';


type Props = {
  fetchCoinsData: ({limit: number}) => void,
  limit: number,
};

const LimitSelect = ({fetchCoinsData, limit}: Props) => {

  // Limit options used in select
  const limitOptions = [
    {
      label: '10',
      value: 10,
    },
    {
      label: '50',
      value: 50,
    },
    {
      label: 'all',
      value: '',
    },
  ];

  // Fetch data on mount
  useEffect(() => {
    fetchCoinsData();
  }, [fetchCoinsData]);


  // Fetch data on select change
  const handleSelectChange = (e) => {
    fetchCoinsData(e.target.value);
  }

  return(
    <div className={'limitSelect'}>
      <label htmlFor={'limitSelect'}>Limit</label>
      <select defaultValue={limit} id={'limitSelect'} onChange={handleSelectChange}>
        {limitOptions.map(opt => <option value={opt.value} key={opt.label}>{opt.label}</option>)}
      </select>
    </div>
  );
}

export default LimitSelect;
